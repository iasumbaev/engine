<?php

namespace App\Entity;

use App\Entity\Traits\HiddenTrait;
use App\Entity\Traits\ImageTrait;
use App\Entity\Traits\MetaTrait;
use App\Entity\Traits\PositionTrait;
use App\Entity\Traits\SlugTrait;
use App\Entity\Traits\TextTrait;
use App\Entity\Traits\TitleTrait;
use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    use SlugTrait;
    use PositionTrait;

    use TitleTrait;
    use TextTrait;
    use MetaTrait;
    use HiddenTrait;
    use ImageTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=ImageFile::class, mappedBy="article")
     * @ORM\OrderBy({"position"="ASC"})
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=SlideFile::class, mappedBy="article")
     * @ORM\OrderBy({"position"="ASC"})
     */
    private $slides;

    /**
     * @ORM\OneToMany(targetEntity=DocumentFile::class, mappedBy="article")
     * @ORM\OrderBy({"position"="ASC"})
     */
    private $documents;

    /**
     * @ORM\OneToOne(targetEntity=DocumentFile::class, cascade={"persist", "remove"})
     */
    private $document;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->slides = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|ImageFile[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ImageFile $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setArticle($this);
        }

        return $this;
    }

    public function removeImage(ImageFile $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getArticle() === $this) {
                $image->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SlideFile[]
     */
    public function getSlides(): Collection
    {
        return $this->slides;
    }

    public function addSlide(SlideFile $slide): self
    {
        if (!$this->slides->contains($slide)) {
            $this->slides[] = $slide;
            $slide->setArticle($this);
        }

        return $this;
    }

    public function removeSlide(SlideFile $slide): self
    {
        if ($this->slides->contains($slide)) {
            $this->slides->removeElement($slide);
            // set the owning side to null (unless already changed)
            if ($slide->getArticle() === $this) {
                $slide->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DocumentFile[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(DocumentFile $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setArticle($this);
        }

        return $this;
    }

    public function removeDocument(DocumentFile $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getArticle() === $this) {
                $document->setArticle(null);
            }
        }

        return $this;
    }

    public function getDocument(): ?DocumentFile
    {
        return $this->document;
    }

    public function setDocument(?DocumentFile $document): self
    {
        $this->document = $document;

        return $this;
    }


}
