<?php


namespace App\Entity\Traits;


trait SlugTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $slug;

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

}