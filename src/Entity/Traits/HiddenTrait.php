<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait HiddenTrait
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $hidden = false;

    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;

        return $this;
    }
}
