<?php

namespace App\Entity\Traits;

use App\Entity\ImageFile;
use Doctrine\ORM\Mapping as ORM;

trait ImageTrait
{
    /**
     * @ORM\OneToOne(targetEntity=ImageFile::class, cascade={"persist", "remove"})
     */
    private $image;

    public function getImage(): ?ImageFile
    {
        return $this->image;
    }

    public function setImage(?ImageFile $image): self
    {
        $this->image = $image;

        return $this;
    }
}
