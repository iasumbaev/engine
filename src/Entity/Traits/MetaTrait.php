<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait MetaTrait
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="100")
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", nullable=true,  length=255)
     * @Assert\Length(max="250")
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", nullable=true,  length=255)
     * @Assert\Length(max="250")
     */
    private $metaKeywords;

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param mixed $metaKeywords
     */
    public function setMetaKeywords($metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

}
