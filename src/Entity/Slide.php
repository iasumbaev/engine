<?php

namespace App\Entity;

use App\Entity\Traits\HiddenTrait;
use App\Entity\Traits\ImageTrait;
use App\Entity\Traits\LinkTrait;
use App\Entity\Traits\PositionTrait;
use App\Entity\Traits\TextTrait;
use App\Entity\Traits\TitleTrait;
use App\Repository\SlideRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SlideRepository::class)
 */
class Slide
{
    use TitleTrait;
    use TextTrait;
    use LinkTrait;
    use ImageTrait;
    use PositionTrait;
    use HiddenTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
