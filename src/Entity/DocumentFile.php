<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=DocumentFileRepository::class)
 */
class DocumentFile extends AppFile
{
    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="documents")
     */
    private $article;

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }
}
