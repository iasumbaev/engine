<?php


namespace App\Entity;

use App\Entity\Traits\PositionTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class AppFile
{
    use PositionTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("main")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("main")
     */
    protected $filename;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Groups("main")
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     * @Groups("main")
     */
    protected $originalFilename;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("main")
     */
    protected $mimeType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $extension;

    /**
     * @ORM\Column(type="bigint")
     * @Groups("main")
     */
    protected $size;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"main", "input"})
     */
    protected $viewFilename;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(string $originalFilename): self
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }


    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;
        return $this;
    }

    public function getSizeInUnit(): ?string
    {
        if ($this->size < 1024) {
            return $this->size . ' б';
        }

        if ($this->size < 1024 * 1024) {
            return round($this->size / 1024) . ' Кб';
        }

        if ($this->size < 1024 * 1024 * 1024) {
            return round($this->size / 1024 / 1024) . ' Мб';
        }
        return $this->size . ' б';
    }

    public function getViewFilename(): ?string
    {
        return $this->viewFilename;
    }

    public function setViewFilename(string $viewFilename): self
    {
        $this->viewFilename = $viewFilename;

        return $this;
    }

    public function getFilePath(): string
    {
        return $this->path . '/' . $this->filename;
    }
}