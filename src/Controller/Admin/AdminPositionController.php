<?php

namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/%admin_hash%/admin")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminPositionController extends AbstractController
{
    /**
     * @Route("/change_position/{class}", name="admin_change_position", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $class
     * @return Response
     */
    public function changePosition(Request $request, EntityManagerInterface $entityManager, $class): Response
    {
        $classType = $class;

        $data = $request->request->all();
        foreach ($data as $index => $id) {
            $item = $entityManager->getRepository($classType)->find($id);
            if ($item !== null) {
                $item->setPosition($index + 1);
                $entityManager->persist($item);
            }
        }
        $entityManager->flush();

        return new Response('success');
    }
}