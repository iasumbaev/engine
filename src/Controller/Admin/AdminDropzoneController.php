<?php

namespace App\Controller\Admin;


use App\Entity\DocumentFile;
use App\Entity\ImageFile;
use App\Entity\SlideFile;
use App\Service\AppFileService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/%admin_hash%/admin/dropzone", name="admin_dropzone_")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminDropzoneController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AppFileService
     */
    private $appFileService;

    public function __construct(EntityManagerInterface $entityManager, AppFileService $appFileService)
    {
        $this->entityManager = $entityManager;
        $this->appFileService = $appFileService;
    }


    /**
     * @Route("/{class}/{id}/images", name="add_image", defaults={"_type"="image"}, methods={"POST"})
     * @Route("/{class}/{id}/slides", name="add_slide", defaults={"_type"="slide"}, methods={"POST"})
     * @Route("/{class}/{id}/documents", name="add_document", defaults={"_type"="document"}, methods={"POST"})
     * @param Request $request
     * @param $class
     * @param $id
     * @param string $_type
     * @return JsonResponse
     */
    public function uploadAppFile(Request $request, $class, $id, string $_type): JsonResponse
    {
        /**
         * @var ServiceEntityRepository $repository
         */
        $repository = $this->entityManager->getRepository($class);

        if ($repository) {
            $parent = $repository->find($id);
            if ($parent) {
                if ($this->appFileService->uploadAppFile($parent, $request, $result, $_type)) {
                    return $this->json(
                        $result,
                        201,
                        [],
                        [
                            'groups' => ['main']
                        ]
                    );
                }
                return $this->json($result, 400);
            }
        }

        return $this->json(['Not found repository or object!'], 400);
    }

    /**
     * @Route("/{class}/{id}/images/reorder", defaults={"_type"="image"}, methods="POST", name="reorder_images")
     * @Route("/{class}/{id}/slides/reorder", defaults={"_type"="slide"}, methods="POST", name="reorder_slides")
     * @Route("/{class}/{id}/documents/reorder", defaults={"_type"="document"}, methods="POST", name="reorder_documents")
     * @param Request $request
     * @param $class
     * @param $id
     * @param string $_type
     * @return JsonResponse
     */
    public function reorderAppFiles(Request $request, $class, $id, string $_type): JsonResponse
    {
        /**
         * @var ServiceEntityRepository $repository
         */
        $repository = $this->entityManager->getRepository($class);

        if ($repository) {
            $parent = $repository->find($id);
            if ($parent) {
                if ($this->appFileService->reorderAppFiles($parent, $request, $_type)) {
                    return $this->json(
                        $this->getFilesByType($parent, $_type),
                        200,
                        [],
                        [
                            'groups' => ['main']
                        ]
                    );
                }
                return $this->json(['detail' => 'Invalid body'], 400);
            }
        }

        return $this->json(['Not found repository or object!'], 400);
    }

    /**
     * @Route("/{class}/{id}/images", methods="GET", defaults={"_type"="image"}, name="image_list")
     * @Route("/{class}/{id}/slides", methods="GET", defaults={"_type"="slide"}, name="slide_list")
     * @Route("/{class}/{id}/documents", methods="GET", defaults={"_type"="document"}, name="document_list")
     * @param $class
     * @param $id
     * @param string $_type
     * @return JsonResponse
     */
    public function getAppFiles($class, $id, string $_type): JsonResponse
    {
        /**
         * @var ServiceEntityRepository $repository
         */
        $repository = $this->entityManager->getRepository($class);

        if ($repository) {
            $parent = $repository->find($id);
            if ($parent) {
                return $this->json(
                    $this->getFilesByType($parent, $_type),
                    200,
                    [],
                    [
                        'groups' => ['main']
                    ]
                );
            }
        }

        return $this->json(['Not found repository or object!'], 400);
    }

    /**
     * @Route("/{class}/{parent_id}/images/{id}", name="delete_image", defaults={"_type"="image"}, methods={"DELETE"})
     * @Route("/{class}/{parent_id}/slides/{id}", name="delete_slide", defaults={"_type"="slide"}, methods={"DELETE"})
     * @Route("/{class}/{parent_id}/documents/{id}", name="delete_document", defaults={"_type"="document"}, methods={"DELETE"})
     * @param int $id
     * @param string $_type
     * @param AppFileService $appFileService
     * @return Response
     */
    public function deleteAppFile(int $id, string $_type, AppFileService $appFileService): Response
    {
        switch ($_type) {
            case 'slide':
                $file = $this->entityManager->getRepository(SlideFile::class)->find($id);
                break;
            case 'image':
                $file = $this->entityManager->getRepository(ImageFile::class)->find($id);
                break;
            case 'document':
                $file = $this->entityManager->getRepository(DocumentFile::class)->find($id);
                break;
            default:
                return new Response('Undefined type', 400);

        }
        return $appFileService->deleteAppFile($file);
    }

    /**
     * @Route("/{class}/{parent_id}/slides/{id}", defaults={"_type"="slide"}, name="update_slide", methods={"PUT"})
     * @Route("/{class}/{parent_id}/documentы/{id}", defaults={"_type"="document"}, name="update_document", methods={"PUT"})
     * @param int $id
     * @param Request $request
     * @param string $_type
     * @return JsonResponse
     */
    public function updateAppFile(int $id, Request $request, string $_type): JsonResponse
    {
        switch ($_type) {
            case 'slide':
                $file = $this->entityManager->getRepository(SlideFile::class)->find($id);
                break;
            case 'image':
                $file = $this->entityManager->getRepository(ImageFile::class)->find($id);
                break;
            case 'document':
                $file = $this->entityManager->getRepository(DocumentFile::class)->find($id);
                break;
            default:
                return $this->json('Undefined type', 400);

        }
        if ($this->appFileService->updateAppFile($file, $request, $result, $_type)) {
            return $this->json(
                $file,
                201,
                [],
                [
                    'groups' => ['main']
                ]
            );
        }
        return $this->json($result, 400);

    }

    private function getFilesByType($parent, $type = 'image')
    {
        switch ($type) {
            case 'slide':
                return $parent->getSlides();
            case 'document':
                return $parent->getDocuments();
            default:
                return $parent->getImages();
        }


    }
}