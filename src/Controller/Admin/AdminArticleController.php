<?php

namespace App\Controller\Admin;


use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/%admin_hash%/admin/article", name="admin_article_")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminArticleController extends AbstractController
{
    use AdminControllerTrait;

    /**
     * @Route("/list", name="list")
     * @param ArticleRepository $repository
     * @return Response
     */
    public function list(ArticleRepository $repository): Response
    {
        return $this->render(
            'admin/list.html.twig',
            [
                'items' => $repository->findBy([], ['position' => 'ASC']),
                'create_path' => 'admin_article_create',
                'edit_path' => 'admin_article_edit',
                'delete_path' => 'admin_article_delete',
                'title' => 'Статьи',
                'sortable' => true,
                'class' => Article::class
            ]
        );
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        return $this->renderOrRedirect(
            $request,
            new Article(),
            'admin_article_list',
            'admin/form.html.twig',
            ArticleType::class,
            [],
            ['title' => 'Создание'],
            [
                'imageFile',
                'documentFile'
            ]

        );
    }

    /**
     * @Route("/{id}/edit", name="edit")
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function edit(Request $request, Article $article): Response
    {

        return $this->renderOrRedirect(
            $request,
            $article,
            'admin_article_list',
            'admin/form.html.twig',
            ArticleType::class,
            [],
            [
                'fluid' => true,
                'title' => $article->getTitle(),
                'includeImage' => true,
                'includeDocument' => true,
                'includeDropzone' => true,

                'dropzone_class' => Article::class,
//                'dropzone_type' => 'image',
//                'dropzone_add' => 'admin_dropzone_add_image',
//                'dropzone_list' => 'admin_dropzone_image_list',


//                'dropzone_type' => 'slide',
//                'dropzone_add' => 'admin_dropzone_add_slide',
//                'dropzone_list' => 'admin_dropzone_slide_list',

                'dropzone_type' => 'document',
                'dropzone_add' => 'admin_dropzone_add_document',
                'dropzone_list' => 'admin_dropzone_document_list',

            ],
            [
                'imageFile',
                'documentFile'
            ]
        );
    }

    /**
     * @Route("{id}/delete", name="delete")
     * @param Article $article
     * @return Response
     */
    public function delete(Article $article): Response
    {
        $this->entityManager->remove($article);
        $this->entityManager->flush();
        $this->addFlash('danger', 'Элемент удалён!');
        return $this->redirectToRoute('admin_article_list');
    }


}
