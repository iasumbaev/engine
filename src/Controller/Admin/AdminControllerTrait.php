<?php


namespace App\Controller\Admin;


use App\Service\FormSubmitHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

trait AdminControllerTrait
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FormSubmitHelper
     */
    private $formSubmitHelper;

    public function __construct(EntityManagerInterface $entityManager, FormSubmitHelper $formSubmitHelper)
    {
        $this->entityManager = $entityManager;
        $this->formSubmitHelper = $formSubmitHelper;
    }


    private function initEntity(ServiceEntityRepository $repository, $object)
    {
        $data = $repository->findOneBy([]);
        if (!$data) {
            $data = $object;
            $this->entityManager->persist($data);
            $this->entityManager->flush();
        }

        return $data;
    }

    private function initForm(string $type, $data, Request $request, array $options = []): FormInterface
    {
        /**
         * @var FormInterface $form
         */
        $form = $this->createForm($type, $data, $options);
        $form->add('submit', SubmitType::class, [
            'label' => 'Принять',
            'attr' => [
                'class' => 'btn-success'
            ]
        ]);
        $form->handleRequest($request);
        return $form;
    }

    private function renderOrRedirect(Request $request,
                                      $data,
                                      string $redirectRoute,
                                      string $renderView,
                                      string $formType,
                                      array $redirectParameters = [],
                                      array $renderParameters = [],
                                      array $formFields = [],
                                      array $formOptions = []

    )
    {
        $form = $this->initForm($formType, $data, $request, $formOptions);
        if ($this->formSubmitHelper->validateAndSubmitForm($form, $formFields)) {
            $this->addFlash('success', 'Изменения успешно сохранены');
            return $this->redirectToRoute($redirectRoute,
                $redirectParameters);
        }

        $renderParameters['form'] = $form->createView();
        return $this->render($renderView, $renderParameters);
    }


}