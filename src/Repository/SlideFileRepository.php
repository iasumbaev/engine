<?php

namespace App\Repository;

use App\Entity\SlideFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SlideFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method SlideFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method SlideFile[]    findAll()
 * @method SlideFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SlideFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SlideFile::class);
    }

    // /**
    //  * @return SlideFile[] Returns an array of SlideFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SlideFile
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
