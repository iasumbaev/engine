<?php


namespace App\Service;

use App\Entity\AppFile;
use App\Entity\DocumentFile;
use App\Entity\ImageFile;
use App\Entity\SlideFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AppFileService
{

    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(ValidatorInterface $validator, UploaderHelper $uploaderHelper, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->validator = $validator;
        $this->uploaderHelper = $uploaderHelper;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;

    }

    public function uploadAppFile($data, Request $request, &$result, $type = 'image'): bool
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('reference');

        switch ($type) {
            case 'image':
            case 'slide':
                $violations = $this->validator->validate(
                    $uploadedFile,
                    [
                        new NotBlank([
                            'message' => '�������� ���� ��� ��������!'
                        ]),
                        new File([
                            'maxSize' => '8M',
                            'mimeTypes' => [
                                'image/jpeg',
                                'image/png',
                                'image/gif'
                            ]
                        ])
                    ]
                );
                break;
            case 'document':
                $violations = $this->validator->validate(
                    $uploadedFile,
                    [
                        new NotBlank([
                            'message' => '�������� ���� ��� ��������!'
                        ]),
                        new File([
                            'maxSize' => '20M',
                            'mimeTypes' => [
                                'application/pdf',
                                'application/msword',
                                'application/vnd.ms-excel',
                                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                                'application/vnd.ms-powerpoint',
                                'text/plain'
                            ]
                        ])
                    ]
                );
                break;

            default:
                $result = ['Undefined type'];
                return false;
        }


        if ($violations->count() > 0) {
            $result = $violations;
            return false;
        }


        switch ($type) {
            case 'image':
                $result = $this->uploaderHelper->uploadAppFile($uploadedFile, new ImageFile(), 'image');
                if (method_exists($data, 'addImage')) {
                    $data->addImage($result);
                }
                break;
            case 'slide':
                $result = $this->uploaderHelper->uploadAppFile($uploadedFile, new SlideFile(), 'slide');
                if (method_exists($data, 'addSlide')) {
                    $data->addSlide($result);
                }
                break;
            case 'document':
                $result = $this->uploaderHelper->uploadAppFile($uploadedFile, new DocumentFile(), 'document');
                if (method_exists($data, 'addDocument')) {
                    $data->addDocument($result);
                }
                break;
        }
        $this->entityManager->persist($result);

        if (is_file($uploadedFile->getPathname())) {
            unlink($uploadedFile->getPathname());
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return true;
    }

    public function updateAppFile($data, Request $request, &$result, $type = 'document'): bool
    {
        switch ($type) {
            case 'slide':
                $classType = SlideFile::class;
                break;
            default:
                $classType = DocumentFile::class;
                break;
        }
        $this->serializer->deserialize(
            $request->getContent(),
            $classType,
            'json',
            [
                'object_to_populate' => $data,
                'groups' => ['input']
            ]
        );

        $violations = $this->validator->validate($data);
        if ($violations->count() > 0) {
            $result = $violations;
            return false;
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        $result = $data;
        return true;
    }

    public function reorderAppFiles($data, Request $request, $type = 'image'): bool
    {
        $orderedIds = json_decode($request->getContent(), true);

        if ($orderedIds === null) {
            return false;
        }
        $orderedIds = array_flip($orderedIds);

        switch ($type) {
            case 'image':
                // from (position)=>(id) to (id)=>(position)
                if (method_exists($data, 'getImages')) {
                    foreach ($data->getImages() as $image) {
                        $image->setPosition($orderedIds[$image->getId()]);
                    }
                    $this->entityManager->flush();
                } else {
                    return false;
                }
                break;
            case 'slide':
                // from (position)=>(id) to (id)=>(position)
                if (method_exists($data, 'getSlides')) {
                    foreach ($data->getSlides() as $slide) {
                        $slide->setPosition($orderedIds[$slide->getId()]);
                    }
                    $this->entityManager->flush();
                } else {
                    return false;
                }
                break;
            case 'document':
                // from (position)=>(id) to (id)=>(position)
                if (method_exists($data, 'getDocuments')) {
                    foreach ($data->getDocuments() as $slide) {
                        $slide->setPosition($orderedIds[$slide->getId()]);
                    }
                    $this->entityManager->flush();
                } else {
                    return false;
                }
                break;
        }

        return true;
    }


    public function deleteAppFile(AppFile $file): Response
    {
        $this->entityManager->remove($file);
        $this->entityManager->flush();

        $this->uploaderHelper->deleteFile($file->getFilePath());

        return new Response(null, 204);
    }


}
