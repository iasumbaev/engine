<?php

namespace App\Service;

use App\Entity\DocumentFile;
use App\Entity\ImageFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\Form\FormInterface;

class FormSubmitHelper
{
    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminSliderController constructor.
     * @param UploaderHelper $uploaderHelper
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UploaderHelper $uploaderHelper, EntityManagerInterface $entityManager)
    {
        $this->uploaderHelper = $uploaderHelper;
        $this->entityManager = $entityManager;
    }

    private function submitField(FormInterface $form, $field, $formField): void
    {
        $data = $form->getData();
        switch ($field) {
            case 'imageFile':
                if (method_exists($data, 'getImage')) {
                    $image = $this->uploaderHelper->uploadAppFile(
                        $formField,
                        new ImageFile(),
                        'image',
                        $data->getImage() ? $data->getImage()->getFilename() : null
                    );
                } else {
                    throw new LogicException('$data has not method getImage');
                }

                if (method_exists($data, 'setImage')) {
                    $data->setImage($image);
                } else {
                    throw new LogicException('$data has not method setImage');
                }
                break;
            case 'documentFile':
                if (method_exists($data, 'getDocument')) {
                    $document = $this->uploaderHelper->uploadAppFile(
                        $formField,
                        new DocumentFile(),
                        'document',
                        $data->getDocument() ? $data->getDocument()->getFilename() : null
                    );
                } else {
                    throw new LogicException('$data has not method getDocument');
                }

                if (method_exists($data, 'setDocument')) {
                    $data->setDocument($document);
                } else {
                    throw new LogicException('$data has not method setDocument');
                }
                break;
        }
    }

    public function validateAndSubmitForm(FormInterface $form, ?array $formFields = null): bool
    {
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            foreach ($formFields as $index => $formField) {
                if (isset($form[$formField]) && $form[$formField]->getData()) {
                    $this->submitField($form, $formField, $form[$formField]->getData());
                }
            }

            $this->entityManager->persist($data);
            $this->entityManager->flush();
            return true;
        }

        return false;
    }

}