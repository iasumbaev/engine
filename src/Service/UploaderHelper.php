<?php


namespace App\Service;


use App\Entity\AppFile;
use App\Entity\ImageFile;
use App\Entity\Slide;
use App\Entity\Video;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class UploaderHelper
{
    private $filesystem;


    private $requestStackContext;

    private $logger;

    private $publicAssetBaseUrl;
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(FilesystemInterface $uploadsFilesystem, RequestStackContext $requestStackContext, LoggerInterface $logger, SluggerInterface $slugger, string $uploadedAssetsBaseUrl)
    {
        $this->filesystem = $uploadsFilesystem;
        $this->requestStackContext = $requestStackContext;
        $this->logger = $logger;
        $this->publicAssetBaseUrl = $uploadedAssetsBaseUrl;
        $this->slugger = $slugger;
    }

    /**
     * @param File $file
     * @param string|null $existingFilename
     * @param $directory
     * @return string
     */
    private function uploadFile(File $file, ?string $existingFilename, $directory): string
    {
        $newFilename = $this->upload($file, $directory, true);

        if ($existingFilename) {
            try {
                $result = $this->filesystem->delete($directory . '/' . $existingFilename);

                if ($result === false) {
                    throw new RuntimeException(sprintf('Could not delete old uploaded file "%s"', $existingFilename));
                }
            } catch (FileNotFoundException $e) {
                $this->logger->alert(sprintf('Old uploaded file "%s" was missing when trying to delete', $existingFilename));
            }
        }

        return $newFilename;
    }

    private function initFile(File $file, AppFile $appFile, $newFilename, $directory): AppFile
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
        } else {
            $originalFilename = $file->getFilename();
            $extension = $file->getExtension();
        }


        $appFile
            ->setFilename($newFilename)
            ->setOriginalFilename($originalFilename)
            ->setViewFilename($originalFilename)
            ->setMimeType($file->getMimeType() ?? 'application/octet-stream')
            ->setExtension($extension)
            ->setPath($directory)
            ->setSize($file->getSize());

        return $appFile;
    }

    public function uploadAppFile(File $file, AppFile $appFile, $directory, ?string $existingFilename = null): AppFile
    {
        $newFilename = $this->uploadFile($file, $existingFilename, $directory);

        if ($appFile instanceof ImageFile || $appFile instanceof SlideFile) {
            [$width, $height] = @getimagesize($this->publicAssetBaseUrl . '/' . $directory . '/' . $newFilename);

            $appFile
                ->setWidth($width)
                ->setHeight($height);
        }

        return $this->initFile($file, $appFile, $newFilename, $directory);
    }


    public function getPublicPath(string $path): string
    {
        $fullPath = $this->publicAssetBaseUrl . '/' . $path;
        // if it's already absolute, just return
        if (strpos($fullPath, '://') !== false) {
            return $fullPath;
        }
        // needed if you deploy under a subdirectory
        return $this->requestStackContext
                ->getBasePath() . $fullPath;
    }

    public function deleteFile(string $path): void
    {
        try {
            $result = $this->filesystem->delete($path);
            if ($result === false) {
                throw new RuntimeException(sprintf('Error deleting "%s"', $path));
            }
        } catch
        (FileNotFoundException $e) {
            $this->logger->alert(sprintf('File "%s" was missing when trying to delete', $path));
        }
    }

    private function upload(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }

        $newFilename = $this->slugger->slug(pathinfo($originalFilename, PATHINFO_FILENAME), '_') . '-' . uniqid('', true) . '.' . $file->guessExtension();

        $stream = fopen($file->getPathname(), 'rb');
        try {
            $result = $this->filesystem->writeStream(
                $directory . '/' . $newFilename,
                $stream,
                [
                    'visibility' => $isPublic ? AdapterInterface::VISIBILITY_PUBLIC : AdapterInterface::VISIBILITY_PRIVATE
                ]
            );
        } catch (FileExistsException $e) {
            throw new RuntimeException(sprintf('Could not write uploaded file "%s"', $newFilename));
        }

        if ($result === false) {
            throw new RuntimeException(sprintf('Could not write uploaded file "%s"', $newFilename));
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $newFilename;
    }


}