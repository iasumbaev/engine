<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    use FormTrait;
    use ConstraintsTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', FileType::class, [
                'attr' => [
                    'placeholder' => 'Выберите изображение'
                ],
                'mapped' => false,
                'required' => true,
                'constraints' => $this->getImageConstraints()
            ])
            ->add('documentFile', FileType::class, [
                'attr' => [
                    'placeholder' => 'Выберите документ'
                ],
                'mapped' => false,
                'required' => true,
                'constraints' => $this->getDocumentConstraints()
            ])
            ->add('title')
            ->add('text', null, [
                'attr' => [
                    'class' => 'editor'
                ]
            ])
            ->add('metaTitle')
            ->add('metaDescription')
            ->add('metaKeywords')
            ->add('hidden');

        $isSluggable = $options['sluggable'];
        $writeMetaTitle = $options['write_meta_title'];
        $this->addEventListner($builder, $isSluggable, $writeMetaTitle);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'sluggable' => true,
            'write_meta_title' => true
        ]);
    }
}
