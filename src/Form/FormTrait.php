<?php


namespace App\Form;


use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\String\Slugger\SluggerInterface;

trait FormTrait
{
    private $slugger;

    // Form types are services, so you can inject other services in them if needed
    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    private function addEventListner(FormBuilderInterface $builder, bool $isSluggable, bool $writeMetaTitle): void
    {
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($isSluggable, $writeMetaTitle) {
            if($isSluggable) {
                $data = $event->getData();
                if (null !== $title = $data->getTitle()) {
                    $data->setSlug($this->slugger->slug($title)->lower());
                }
            }

            if($writeMetaTitle) {
                $data = $event->getData();
                if (null === $data->getMetaTitle()) {
                    $data->setMetaTitle($data->getTitle());
                }
            }
        });
    }
}