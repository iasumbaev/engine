<?php


namespace App\Form;


use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

trait ConstraintsTrait
{
    private function getImageConstraints(bool $checkOnNull = false): ?array
    {
        $imageConstraints = [
            new  Image([
                'maxSize' => '8M',
                'mimeTypes' => [
                    'image/jpeg',
                    'image/png',
                ]
            ])
        ];

        if ($checkOnNull) {
            $imageConstraints[] = new NotNull([
                'message' => 'Пожалуйста, загрузите изображение'
            ]);
        }

        return $imageConstraints;


    }

    private function getSVGConstraints(bool $checkOnNull = false): ?array
    {
        $imageConstraints = [
            new  File([
                'maxSize' => '8M',
                'mimeTypes' => [
                    'image/svg+xml',
                    'image/svg',
                ]
            ])
        ];

        if ($checkOnNull) {
            $imageConstraints[] = new NotNull([
                'message' => 'Пожалуйста, загрузите изображение'
            ]);
        }

        return $imageConstraints;


    }

    private function getDocumentConstraints(bool $checkOnNull = false): ?array
    {
        $imageConstraints = [
            new  File([
                'maxSize' => '8M',
                'mimeTypes' => [
                    'application/pdf',
                    'application/msword',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'application/vnd.ms-powerpoint',
                    'text/plain'
                ]
            ])
        ];

        if ($checkOnNull) {
            $imageConstraints[] = new NotNull([
                'message' => 'Пожалуйста, загрузите файл'
            ]);
        }

        return $imageConstraints;


    }

    private function getVideoConstraints(): array
    {
        return [
            new  File([
                'maxSize' => '20M',
                'mimeTypes' => [
                    'video/mp4',
                    'image/png',
                ]
            ])
        ];
    }
}