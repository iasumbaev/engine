<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201003134851 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slide_file ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slide_file ADD CONSTRAINT FK_BB5FD8C47294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_BB5FD8C47294869C ON slide_file (article_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slide_file DROP FOREIGN KEY FK_BB5FD8C47294869C');
        $this->addSql('DROP INDEX IDX_BB5FD8C47294869C ON slide_file');
        $this->addSql('ALTER TABLE slide_file DROP article_id');
    }
}
