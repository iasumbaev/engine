<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201001124357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE image_file ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_file ADD CONSTRAINT FK_7EA5DC8E7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_7EA5DC8E7294869C ON image_file (article_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE image_file DROP FOREIGN KEY FK_7EA5DC8E7294869C');
        $this->addSql('DROP INDEX IDX_7EA5DC8E7294869C ON image_file');
        $this->addSql('ALTER TABLE image_file DROP article_id');
    }
}
