<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201003121629 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slide_file ADD filename VARCHAR(255) NOT NULL, ADD path VARCHAR(1000) NOT NULL, ADD original_filename VARCHAR(255) NOT NULL, ADD mime_type VARCHAR(255) NOT NULL, ADD extension VARCHAR(255) NOT NULL, ADD size BIGINT NOT NULL, ADD view_filename VARCHAR(255) NOT NULL, ADD position INT NOT NULL, ADD title VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slide_file DROP filename, DROP path, DROP original_filename, DROP mime_type, DROP extension, DROP size, DROP view_filename, DROP position, DROP title');
    }
}
