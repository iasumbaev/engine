import Sortable from 'sortablejs';
import Dropzone from 'dropzone';
import 'dropzone/dist/dropzone.css'
import $ from 'jquery';

Dropzone.autoDiscover = false;

$(function () {
    const $referenceList = $('.js-reference-list');
    if ($referenceList[0]) {
        for (var i = 0; i < $referenceList.length; i++) {
            var referenceList = new ReferenceList($($referenceList[i]));
            initializeDropzone(referenceList, $($referenceList[i]).parent().find('.js-reference-dropzone').get(0));
        }
    }

});

class ReferenceList {
    constructor($element) {
        this.$element = $element;
        this.sortable = Sortable.create(this.$element[0], {
            handle: '.drag-handle',
            animation: 150,
            onEnd: () => {
                console.log(JSON.stringify(this.sortable.toArray()));
                $.ajax({
                    url: this.$element.data('url') + '/reorder',
                    method: 'POST',
                    data: JSON.stringify(this.sortable.toArray())
                });
            }
        });
        this.type = this.$element.data('type');
        this.references = [];

        this.render();

        this.$element.on('click', '.js-reference-delete', (event) => {
            this.handleReferenceDelete(event);
        });

        this.$element.on('blur', '.js-edit-filename', (event) => {
            this.handleReferenceEditFilename(event);
        });

        this.$element.on('blur', '.js-edit-text', (event) => {
            this.handleReferenceEditText(event);
        });

        $.ajax({
            url: this.$element.data('url')
        }).then(data => {
            this.references = data;
            this.render();
        })
    }

    addReference(reference) {
        $.ajax({
            url: this.$element.data('url')
        }).then(data => {
            this.references = data;
            this.render();
        })
    }

    handleReferenceDelete(event) {
        const $li = $(event.currentTarget).closest('.list-group-item');
        const id = $li.data('id');
        $li.addClass('disabled');

        $.ajax({
            url: this.$element.data('url') + '/' + id,
            method: 'DELETE'
        }).then(() => {
            this.references = this.references.filter(reference => {
                return reference.id !== id;
            });
            this.render();
        });
    }

    handleReferenceEditFilename(event) {
        const $li = $(event.currentTarget).closest('.list-group-item');
        const id = $li.data('id');
        const reference = this.references.find(reference => {
            return reference.id === id;
        });
        reference.viewFilename = $(event.currentTarget).val();

        $.ajax({
            url: this.$element.data('url') + '/' + id,
            method: 'PUT',
            data: JSON.stringify(reference)
        });
    }

    handleReferenceEditText(event) {
        const $li = $(event.currentTarget).closest('.list-group-item');
        const id = $li.data('id');
        const reference = this.references.find(reference => {
            return reference.id === id;
        });
        reference.text = $(event.currentTarget).val();

        $.ajax({
            url: this.$element.data('url') + '/' + id,
            method: 'PUT',
            data: JSON.stringify(reference)
        });
    }

    render() {
        const itemsHtml = this.references.map(reference => {
            var element;
            if (this.type === 'image') {
                element = `<img src="/uploads/image/${reference.filename}" alt="" class="w-50">`
            } else if (this.type === 'slide') {
                var text = reference.text;
                if(text === null) {
                    text = '';
                }
                element = `<img src="/uploads/slide/${reference.filename}" alt="" class="w-50">
                        <input type="text" value="${text}" class="form-control js-edit-text">`
            } else {
                element = `<input type="text" value="${reference.viewFilename}" class="form-control js-edit-filename">`
            }

            return `
<li class="list-group-item d-flex justify-content-between align-items-center" data-id="${reference.id}">
    <span class="drag-handle fa fa-arrows-v mr-3"></span>
    ${element}
    <span>
        <button class="js-reference-delete btn btn-link btn-sm"><span class="fa fa-trash"></span></button>
    </span>
</li>
`
        });

        this.$element.html(itemsHtml.join(''));
    }
}

/**
 * @param {ReferenceList} referenceList
 * @param formElement
 */
function initializeDropzone(referenceList, formElement) {
    if (!formElement) {
        return;
    }
    var dropzone = new Dropzone(formElement, {
        paramName: 'reference',
        init: function () {
            this.on('success', function () {
                referenceList.addReference();
            });

            this.on('error', function (file, data) {
                if (data.detail) {
                    this.emit('error', file, data.detail);
                }
            });
        }
    });
}
