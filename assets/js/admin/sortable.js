import Sortable from 'sortablejs';

$(function () {
    var el = document.getElementsByClassName('sortable')[0];

    if(el) {
        Sortable.create(el, {
            handle: '.drag-handle',
            animation: 150,
            onEnd: () => {
                var ids = {};
                $(el).find('tr').each(function (index) {
                    ids[index] = ($(this).data('id'));
                });
                $.ajax({
                    url: el.dataset.url,
                    method: 'POST',
                    data: ids
                });
            }
        });
    }

});